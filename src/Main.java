import java.awt.Color;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JFrame;
import javax.swing.JPanel;

import model.Fenetre;
import model.FireShot;
import model.Voiture;


public class Main {

	public static void main(String[] args) throws InterruptedException {
	

		final Fenetre fenetre = new Fenetre() ;
		fenetre.setVisible(true);
		
		Timer timer = new Timer() ;
		Date now = new Date() ;
		
		final Voiture voiture = new Voiture(fenetre.POSITION_X_DEPART, fenetre.POSITION_Y_DEPART) ;
		fenetre.setVoiture(voiture);
		
		fenetre.addKeyListener(fenetre);
		now.getTime() ;

		TimerTask timerTask = new TimerTask() {
			
			
			
			@Override
			public void run() {
				for(FireShot shot : fenetre.panneau.shots){
					shot.positionY1 -= 20 ;
				}
				
				voiture.avancer(); 
				fenetre.moveCar(voiture.getPositionX(), voiture.getPositionY());
				System.out.println(voiture);
				
			}
		};
		timer.schedule(timerTask, 1000,50);

	}

}
