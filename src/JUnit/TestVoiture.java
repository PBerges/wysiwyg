package JUnit;

import static org.junit.Assert.*;
import model.Voiture;

import org.junit.Before;
import org.junit.Test;

public class TestVoiture {
	private Voiture voiture;
	
	@Before
	public void setup() throws Exception{
		voiture=new Voiture(10,10);
	}
	
	@Test
	public void NewVoiture() {
		
		assertEquals(10,voiture.getVitesse());
		assertEquals(0,voiture.getPositionX());
	}
	
}
