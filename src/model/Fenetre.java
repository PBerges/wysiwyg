package model;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class Fenetre extends JFrame implements KeyListener{
	
	public Panneau panneau ;
	
	public Voiture voiture ;
	
	
	public static int HAUTEUR = 600 ;
	public static int LARGEUR = 800 ;
	
	public static int HAUTEUR_VOITURE = 120 ;
	public static int LARGEUR_VOITURE = 60 ;
	
	public int POSITION_X_DEPART = (LARGEUR / 2) - (LARGEUR_VOITURE / 2) ; 
	public int POSITION_Y_DEPART = HAUTEUR - HAUTEUR_VOITURE ; 
	
	public Fenetre(){
		super() ;

		this.setTitle("Wysiwyg");
		this.setSize(this.LARGEUR,this.HAUTEUR);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(this.EXIT_ON_CLOSE);
		System.out.println(this.POSITION_X_DEPART);
		System.out.println(this.POSITION_Y_DEPART);
		this.panneau = new Panneau(POSITION_X_DEPART,POSITION_Y_DEPART) ;
		this.setContentPane(this.panneau);     
		
		
		
		
	}
	
	public void setVoiture(Voiture v){
		this.voiture = v ;
	}
	
	public void moveCar(int x1, int y1){
		this.panneau.positionX1 = x1 ;
		this.panneau.positionY1 = y1 ;
		this.panneau.repaint();
	}


	@Override
	public void keyPressed(KeyEvent e) {
		if(e.getKeyCode() == 37) {
			this.voiture.direction = 3 ; // Left
//			panneau.imgPicture = "Image/car_left.png";
		}else if(e.getKeyCode() == 38){
//			panneau.imgPicture = "Image/car_up.png";
			this.voiture.direction = 1 ; // Front
		}else if(e.getKeyCode() == 39){
//			panneau.imgPicture = "Image/car_right.png";
			this.voiture.direction = 2 ; // Right 
		}else if(e.getKeyCode() == 40){
//			panneau.imgPicture = "Image/car_down.png";
			this.voiture.direction = 4 ;
			
		}
		else if (e.getKeyCode()==32)
        {
			this.fire();
        }
		else if (e.getKeyCode()==90)
        {
			this.voiture.accelerer(1);
        }
		else if (e.getKeyCode()==83){
			this.voiture.ralentir(1);
			
		}
		
		
	}
	
	public void fire() {
		
		FireShot shot = new FireShot(this.voiture.getPositionX(), this.voiture.getPositionY());
		this.panneau.shots.add(shot);
		
	}


	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	  

}
