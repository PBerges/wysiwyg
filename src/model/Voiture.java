package model;

import java.awt.Image;


public class Voiture {
	 private int vitesse;
	 private int positionX;
	 private int positionY;
	 public int direction; // 1 tout droit - 2 droite - 3 gauche - 4 arriere

	public Voiture(int positionX, int positionY) {
		super();
		this.vitesse = 10;
		this.positionX = positionX ;
		this.positionY = positionY ;
		this.direction = 1 ;
	}
	
	
	public int getVitesse() {
		return vitesse;
	}
	
	public void setVitesse(int vitesse) {
		this.vitesse = vitesse;
	}
	
	public int getPositionX() {
		return positionX;
	}
	
	public int getPositionY() {
		return positionY;
	}
	
	public void setPositionX(int position) {
		this.positionX = position;
	}
	
	public void setPositionY(int position) {
		this.positionY = position;
	}
	
	public void avancer(){
		if (this.positionX > (Fenetre.LARGEUR - Fenetre.LARGEUR_VOITURE))
        {
            this.positionX = (Fenetre.LARGEUR)-Fenetre.LARGEUR_VOITURE;
//            this.vitesse = 0;
        }
        else if (this.positionX < 0)
        {
            this.positionX = 0;
//            this.vitesse = 0;
        }
        else if (this.positionY > (Fenetre.HAUTEUR - Fenetre.HAUTEUR_VOITURE))
        {
            this.positionY = (Fenetre.HAUTEUR)-Fenetre.HAUTEUR_VOITURE;
//            this.vitesse = 0;
        }
        else if (this.positionY <= 0)
        {
            this.positionY = 0;
//            this.vitesse = 0;

        }
		switch(this.direction){
		case 1: this.positionY -= this.vitesse ; break ; 
		case 2: this.positionX += this.vitesse ; break ;
		case 3: this.positionX -= this.vitesse ; break ;
		case 4: this.positionY += this.vitesse ; break ;
		}
		
	}
	
	
	
	public void accelerer(double v){
		if(this.vitesse+v>200){
			this.vitesse=200;
		}else{
			this.vitesse+=v;
		}
	}
	public void ralentir(double v){
		if(this.vitesse-v< 0){
			this.vitesse= 0;
		}else{
			this.vitesse-=v;
		}
	}
	
	public String toString(){
		String desc = "Vitesse : " ;
		desc += this.vitesse + " Position : " + this.positionY ;
		
		return desc;
		
	}
	
}
